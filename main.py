from ihm import WindowManager, IMAGES, \
    SuperviseurPage, CameraPage, StratPage
from ihm.strat import StrategieManager, Tirette
from strategies import STRAT_DICT

TIRETTE_PIN = 2


window_manager = WindowManager()

Tirette(TIRETTE_PIN)
StrategieManager(STRAT_DICT)


window_manager.build_menu_bar(
    [
        (IMAGES.MENU.CAMERA, [("Visualisation",SuperviseurPage),("Caméra",CameraPage)]),
        (IMAGES.MENU.PLAY, [("Stratégie",StratPage)]),
        (IMAGES.MENU.OFF, WindowManager.validate_destruction)
    ]
)

CameraPage.display()

window_manager.main_loop()