from typing import Dict, Type
from threading import Thread

from ihm import stm, client_mqtt  # , lidar
from ihm.strat import Strategie

from ihm.stm import StmManager, coordinate_system
from ihm.coordinate_system import Position


from ihm.strat import TeamColor


class TestStrat(Strategie):

    def __init__(self, team_color: TeamColor, aruco: Dict[str, int]) -> None:
        """
        This method is called when the strategie is choosen,
        it may automatically move the robot to the desired starting point.
        """
        self._updated = False
        self.commands = [
            Position(0  , 300, 0),
            Position(300, 300, 0),
            Position(300, 0  , 0),
            Position(0  , 0  , 0),

            
            Position(300, 300, 0),
            Position(0  , 300, 0),
            Position(300, 0  , 0),
            Position(0  , 0  , 0),
        ]

        pass

    def update(self):

        if(not self._updated):
            StmManager.stm_manager.go_to(self.current_command, self._receive)
            self._updated = True

        pass

    def _receive(self, args):

        if(args[0] == "Done"):
            if(len(self.commands) == 0):
                self.abort()
                return

            self.current_command = self.commands.pop(0)
            self._updated = False

    def _main_loop(self):
        while len(self.commands) > 0 and not self._abort:
            self.update()

    def start(self):
        self.current_command = self.commands.pop(0)
        self._main_loop_thread = Thread(target=self._main_loop)
        self._abort = False
        self._main_loop_thread.start()

    def abort(self):
        if self._main_loop_thread is not None:
            self._abort = True
            self._main_loop_thread.join()
        StmManager.stm_manager.stop()

    def score(self) -> int:
        pass
    pass


STRAT_DICT: Dict[str, Type[Strategie]] = {
    "Test strat": TestStrat
}
